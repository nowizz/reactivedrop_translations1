﻿GAME
{
	"pos_x"		"-3625"
	"pos_y"		"5315"
	"scale"		"6.0"

	"material" 		"vgui/swarm/overviews/rd_res_forestentranceoverview"
	"briefingmaterial" 	"vgui/swarm/overviews/rd_res_forestentranceoverviewbriefing"
	
	//"missiontitle"		"Transport Facility"
	"missiontitle"		"수송 시설 "
	//"description"		"The Research 7 Transport Facility hasn't been heard from in a week. Investigate the facility."
	"description"		"제 7 연구소 수송 시설에서 일주일 째 소식이 없습니다.  시설을 조사하십시오."

	"image"			"swarm/MissionPics/rd_res_M1MissionPic"

	"builtin" 		"1"
	"version"		"1.3"
	"author"		"Will Rogers"
	"website"		"https://sites.google.com/site/swarmaddons/maps/research7"
}
