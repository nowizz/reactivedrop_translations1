﻿GAME
{
	"pos_x"	"-6331"
	"pos_y"	"2341"
	"scale"	"9"
	"material" "vgui/swarm/Overviews/rydbergOverview"
	"briefingmaterial" "vgui/swarm/Overviews/rydbergBriefingOverview"
	"missiontitle"		"리드버그 반응로 "
	//"description"	"Gain access to the Reactor Control Room and bring the power back online."
	"description"	"반응로 제어실에 접근해 전원을 다시 켜십시오."
	"image"		"swarm/ObjectivePics/obrydbergreactor"
	"version"		"1"
	"author"		"Valve"
	"website"		"http://www.alienswarm.com/"
	"builtin"		"1"
}
