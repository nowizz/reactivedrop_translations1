GAME
{
	"pos_x"	"-3900"
	"pos_y"	"9731"
	"scale"	"12.0"
	"material" "vgui/swarm/Overviews/deimaOverview"
	"briefingmaterial" "vgui/swarm/Overviews/deimaBriefingOverview"
	"missiontitle"		"데이마 표면교 "
	//"description"	"Scans indicate heavy Swarm activity near the pipelines.  Locate compromised pipeline."
	"description"	"배관선로 근처에 심각한 군단 활동이 감지되고 있습니다.  위협이 되는 배관선로를 찾으십시오."
	"image"		"swarm/ObjectivePics/obdeimaesc"
	"version"		"1"
	"author"		"Valve"
	"website"		"http://www.alienswarm.com/"
	"builtin"		"1"
}
