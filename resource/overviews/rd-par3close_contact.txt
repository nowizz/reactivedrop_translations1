﻿GAME
{
	"pos_x"		"-7761"
	"pos_y"		"6266"
	"scale"		"6.5"
	"material" 		"vgui/swarm/Overviews/rd_par3_overview"
	"briefingmaterial" 	"vgui/swarm/Overviews/rd_par3_overview"
	//"missiontitle"		"Close Contact"
	"missiontitle"		"연락 두절 "
	//"description"		"After cleaning the first level, you decide to learn about recent activities of the base."
	"description"		"첫 번째 층을 싹쓸이 한 후, 당신은 기지의 최근 활동들을 알아내기로 결정했습니다."
	"image"			"swarm/objectivepics/rd_par_objectif23"

	"builtin" 		"1"
	"version"		"1"
	"author"		"Eclipse"
	"website"		""
}

// Overview: scale 6.50, pos_x -6929, pos_y 6266
