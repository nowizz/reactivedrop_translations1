GAME
{
	// identifier for this campaign
	"CampaignName" "9800 구역"
	
	// description shown on the main menu when choosing which campaign to play
	"CampaignDescription" "9800 구역"
	
	// texture used on the main menu when choosing which campaign to play
	"ChooseCampaignTexture" "rd-area9800logo/area_9800_main"
	
	// texture used on the campaign screen for the map denk dat dit de overvieuw is
	"CampaignTextureName" "swarm/campaign/rd-area9800_map"
	
	// these textures are overlaid on top of the campaign map in order
	"CampaignTextureLayer1" "swarm/Campaign/rd-area9800_map_haze"			//"CampaignTextureLayer1" "swarm/Campaign/JacobCampaignMap_Haze"
	"CampaignTextureLayer2" "swarm/Campaign/CampaignMap_EmptyLayer"			//"CampaignTextureLayer2" "swarm/Campaign/JacobCampaignMap_SnowNear"
	"CampaignTextureLayer3" "swarm/Campaign/CampaignMap_EmptyLayer"			//"CampaignTextureLayer3" "swarm/Campaign/JacobCampaignMap_SnowFar"
	
	// custom campaign credits file
	"CustomCreditsFile" 	"resource/rd-area9800Credits"

	// position of this campaign in the galactic map (coords go from 0 to 1023)
	"GalaxyX"   "660"
	"GalaxyY"   "262"
	
	// searchlights (optional, max of 4)
	// angle: 0 is right, 90 is up, 180 is left, 270 is down
	//"Searchlight1X" "217"
	//"Searchlight1Y" "860"
	//"Searchlight1Angle" "80"
	//"Searchlight2X" "263"
	//"Searchlight2Y" "751"
	//"Searchlight2Angle" "100"
	//"Searchlight3X" "92"
	//"Searchlight3Y" "446"
	//"Searchlight3Angle" "90"
	//"Searchlight4X" "580"
	//"Searchlight4Y" "266"
	//"Searchlight4Angle" "90"
	
	// first mission entry is a dummy for the starting point
	"MISSION"
	{
		"MissionName"		"Entry Port"
		"MapName"		"start_area"
		"LocationX"		"981"
		"LocationY"		"892"
		"DifficultyModifier" 	"-2"
		"Links"			"rd-area9800LZ" 
		"LocationDescription"  	"Home Base"
		"ShortBriefing"  	"Dropship Bloodhound will enter atmosphere at these co-ordinates and proceed to primary objective."
	}
	
	// each mission listed
	"MISSION"
	{
		"MissionName"		"착륙 구역"		// name used on the map screen, etc.
		"MapName"		"rd-area9800LZ"		// name of the map file
		"LocationX"		"117"				// location of the dot on the map
		"LocationY"		"824"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"start_area rd-area9800PP1"	// map names of neighbours
		"LocationDescription" 	"착륙 구역"
		"ShortBriefing"  	"그 기지에서 무엇이 잘못되었는지 확인하십시오."
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
	"MISSION"
	{
		"MissionName"		"발전소 냉각 펌프"		// name used on the map screen, etc.
		"MapName"		"rd-area9800PP1"		// name of the map file
		"LocationX"		"229"				// location of the dot on the map
		"LocationY"		"422"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"rd-area9800LZ rd-area9800PP2"	// map names of neighbours
		"LocationDescription" 	"발전소 냉각 펌프"
		"ShortBriefing"  	"전원을 가동시키십시오."
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
	"MISSION"
	{
		"MissionName"		"발전소의 발전기"		// name used on the map screen, etc.
		"MapName"		"rd-area9800PP2"		// name of the map file
		"LocationX"		"380"				// location of the dot on the map
		"LocationY"		"380"				// (from 0 to 1023, on the above texture)
		"ThreatString" 		"1"    				// Threat level string to help players decide where to go next
		"Links"			"rd-area9800PP1 rd-area9800WL"	// map names of neighbours
		"LocationDescription" 	"발전소의 발전기"
		"ShortBriefing"  	"전원을 가동시키십시오."
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"				// If set to 1, two players need to be connected to play (no solo play)
	}
	"MISSION"
	{
		"MissionName"	"황무지"
		"MapName"		"rd-area9800WL"
		"LocationX"		"720"
		"LocationY"		"433"
		"ThreatString" 		"1"
		"Links"			"rd-area9800PP2"
		"LocationDescription" 	"Wastelands"
		"ShortBriefing"  	"황무지를 가로지르십시오."
		"AlwaysVisible"		"1"
		"NeedsMoreThanOneMarine" "0"
	}
}	